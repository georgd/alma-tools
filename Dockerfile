FROM catmandu-marc
MAINTAINER Georg Mayr-Duffner "georg.mayr-duffner@wu.ac.at"

RUN cpanm MongoDB JSON::MaybeXS Log::Any::Adapter Log::Log4perl Catmandu::Store::MongoDB utf8::all YAML::AppConfig Modern::Perl XML::LibXML Path::Tiny Moose

COPY . /opt/alma-tools
WORKDIR /opt/alma-tools
COPY crontab /etc/cron/crontab
ENV PERL5LIB=$PERL_PATH:$PERL_PATH/lib/perl5:$PERL5LIB:/opt/alma-tools/lib

RUN crontab /etc/cron/crontab
EXPOSE 80

CMD ["crond", "-f"]
