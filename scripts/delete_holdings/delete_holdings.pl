#!/usr/bin/env perl

use strict;
use warnings;
use 5.014;
use Path::Tiny;
use Data::Dumper qw( Dumper );
use Log::Log4perl qw( get_logger :levels );
use YAML::AppConfig;
use Getopt::Long qw( :config posix_default bundling no_ignore_case );
use Alma::API::Bibs::Holdings;

GetOptions(
    'verbose|v' => \my $verbose,
    'debug|d'   => \my $debug,
    'input|i=s'   => \my $input,
);

my $conf = YAML::AppConfig->new( file => '/opt/alma-tools/conf.yml' );

### Initialize logger ###
my $appender = Log::Log4perl::Appender->new(
    "Log::Dispatch::File",
    filename => "delete_holdings.log",
    mode     => "append",
);
my $errorscreen =
  Log::Log4perl::Appender->new( "Log::Dispatch::Screen", mode => "append", );

#$errorscreen->threshold($ERROR);
$errorscreen->threshold($INFO);
my $layout =
  Log::Log4perl::Layout::PatternLayout->new("%d %p> %F{1}:%L %M - %m%n");
$appender->layout($layout);
$errorscreen->layout($layout);
my $request_logger = get_logger('Alma');
my $logger         = get_logger();
$request_logger->add_appender($appender);
$request_logger->add_appender($errorscreen);
$logger->add_appender($appender);
$logger->add_appender($errorscreen);

if ($verbose) {
    $request_logger->level($INFO);
    $logger->level($INFO);
} elsif ($debug) {
    $request_logger->level($DEBUG);
    $logger->level($DEBUG);
} else {
    $request_logger->level($WARN);
    $logger->level($WARN);
}
### End logger initialization ###

sub main {
    $logger->info('Starting main program');

    my @ids = path($input)->lines( { chomp => 1 } );

    foreach my $line (@ids) {
        my ( $mms_id, $hol_id ) = split ',', $line;
        state $id= 0;
        next unless $mms_id =~ m/9\d+3337/;

        my $hol = Alma::API::Bibs::Holdings->new(
            apikey     => $conf->get_apikeys->{bibs_rw},
            mms_id     => $mms_id,
            holding_id => $hol_id,
            id         => $id++,
        );

        my $resp = $hol->delete();
    }
}

main();
