#!/usr/bin/env perl
######################################################################
# Upload thesis data                                                 #
######################################################################

use Modern::Perl '2016';
use Text::CSV;
use YAML::AppConfig;
use MongoDB;
use Log::Log4perl qw( get_logger :levels );
use Getopt::Long qw( :config posix_default bundling no_ignore_case );
use utf8::all;

use WUW::Bibs::Thesis;
use Alma::API::Bibs;
use Path::Tiny;
use LWP;
use XML::LibXML;

GetOptions(
    'verbose|v' => \my $verbose,
    'debug|d'   => \my $debug,
    'test|t'    => \my $test,
    'staging|s' => \my $staging,
    'input|i=s' => \my $input,
);

sub main {
    my $logger = initialize_logger();
    $logger->info("Started main programme");

    my $conf = load_conf();

    my $db = _mongodb( $conf, 'theseslist' );

    open my $fh, "<:encoding(utf8)", $input or die "$input: $!";
    my $csv  = Text::CSV->new( { binary => 1, sep_char => ";" } );
    my @cols = @{ $csv->getline($fh) };
    my $row  = {};
    $csv->bind_columns( \@{$row}{@cols} );

    my ( %title_list, %bibs );
    while ( $csv->getline($fh) ) {

        # skip any non thesis entry row
        next unless $row->{MATNR} =~ m/\d+/;

        # skip old entries
        my $abschlussdatum_normalized = $row->{ABSCHLUSSDATUM};
        $abschlussdatum_normalized =~ s{(\d{2})\.(\d{2})\.(\d{4})}{$3$2$1};
        next
          if $abschlussdatum_normalized le $conf->get_theses_upload_threshold;

        # build an ID from MATNR and certification date
        my $id = $row->{MATNR} . '_' . $row->{PRF_DATUM};

        # skip record if it was imported already
        if ( my $hss = $db->find_one( { '_id' => $id } ) ) {
            $logger->warn( $id, " by ", $hss->{author},
                " was already uploaded" );
            next;
        }

        # skip record if two records for the same mtnr exist already in Alma
        my $local_id = _build_local_id( $row->{MATNR}, $logger, $conf );
        next unless $local_id =~ m{LJ[HI]\d+};

        # remove any superfluous newline character
        my $title_de = cleanup_title( $row->{TITEL_DE} );
        my $title_en = cleanup_title( $row->{TITEL_EN} );
        $title_en = fake_sentence_case($title_en);

        my $norm_title_id = normalize_title($title_de) . $row->{PRF_DATUM};

        if ( my $_id = $title_list{$norm_title_id} ) {

            # Theses with multiple authors have an entry for each author
            # - add additional authors to an entry that was already read.
            $bibs{$_id}->add_coauthor( $row->{VERFASSERIN} );
        } else {

            # extract years from date entries
            my $year = $row->{PRF_DATUM};
            $year =~ s/.*\.(\d{4})$/$1/;
            my $year_dipl = $row->{ABSCHLUSSDATUM};
            $year_dipl =~ s/.*\.(\d{4})$/$1/;

            # build param list for bib record
            my $params = {
                local_id  => $local_id,
                author    => $row->{VERFASSERIN},
                type      => $row->{ART},
                title_ger => $title_de,
                title_eng => $title_en,
                language  => $row->{TEXTSPRACHE},
                studies   => $row->{STUDIUM},
                reviewer  => $row->{BEUR_ZUNAME} . ', ' . $row->{BEUR_VORNAME},
                year      => $year,
                year_dipl => $year_dipl,
            };
            if ( $row->{BETR_ZUNAME} ) {
                $params->{supervisor} =
                  $row->{BETR_ZUNAME} . ', ' . $row->{BETR_VORNAME};
            }

            $bibs{$id}                  = WUW::Bibs::Thesis->new($params);
            $title_list{$norm_title_id} = $id;
        }
    }

    my @testing_output if $test;
    my $count_successful = 0;
    keys %bibs;
    while ( my ( $id, $bib ) = each %bibs ) {

        my $bib_record = $bib->bibxml;
        push @testing_output, $bib_record if $test;

        my $params = { validate => 'false', override_warning => 'true' };

        my $api_call = Alma::API::Bibs->new(
            params => $params,
            apikey => $conf->get_apikeys->{bibs_rw},
            id     => $id
        );

        if ( $api_call->post($bib_record)->{msg} eq 'OK' ) {
            $db->insert_one(
                {
                    '_id'      => $id,
                    'author'   => $bib->author,
                    'title'    => $bib->title,
                    'local_id' => $bib->local_id
                }
            );
            $count_successful++;
        }

    }

    $logger->warn("$count_successful records uploaded.");
    path('test_output.txt')->spew( join "\n", @testing_output ) if $test;
}

sub cleanup_title {
    my $title = shift;

    $title =~ s{((\s*:\s*)?[\r\n]+)}{ : }g;
    $title =~ s{\.$}{};
    return $title;
}

sub fake_sentence_case {
    my $title    = shift;
    my @uc_words = qw(
      England
      English
      German
      Germany
      Austria
      Austrian
      Finland
      Finnish
      France
      French
      Italy
      Italian
      Russia
      Russian
      China
      Chinese
      Spain
      Spanish
      Europe
      European
      America
      American
      Marckov
      Columbia
      Columbian
      Tanzania
      January
      Feburary
      March
      April
      May
      June
      July
      August
      September
      October
      November
      December
      Monday
      Tuesday
      Wednesday
      Thursday
      Friday
      Saturday
      Sunday
      Willem
      Vis
      Trans-Adriatic-Pipeline
      No-Trans-Adriatic-Pipeline
      Bayesian
      Berlin
      London
      Paris
      Vienna
      Viennese
      Google
    );
    my @words      = split /\s+/, $title;
    my $first_word = shift @words;
    foreach my $w (@words) {
        if ( $w !~ m/\w[A-Z]/ && $w =~ m/\w+/ && !( grep $_ eq $w, @uc_words ) ) {
            $w = lcfirst($w);
        }
    }
    return join " ", $first_word, @words;
}

sub normalize_title {
    my $title = shift;
    return $title =~ s{\w}{}g;
}

sub _mongoCollName {
    my $collName = join ".", @_;
    $collName =~ s/-/_/g;
    return $collName;
}

sub _mongodb {
    my $conf       = shift;
    my $collection = _mongoCollName(@_);
    my $dbName     = $conf->get_app->{theses_db};
    my $dbUrl      = $conf->get_db_url;

    #my $dbPort     = $conf->get_db_port;

    my $client =
      MongoDB::MongoClient->new( host => $dbUrl );    # . ":" . $dbPort );
    my $db = $client->get_database($dbName);
    return $db->get_collection($collection);
}

sub _build_local_id {
    my $matnr    = shift;
    my $logger   = shift;
    my $conf     = shift;
    my $local_id = "LJH" . $matnr;

    return _check_local_id( $local_id, $logger, $conf );
}

sub _check_local_id {
    my $local_id = shift;
    my $logger   = shift;
    my $conf     = shift;

    my $sruUrl =
        $conf->get_alma->{domain}
      . '/view/sru/'
      . $conf->get_alma->{inst_code}
      . '?version=1.2&operation=searchRetrieve'
      . '&query=alma.local_control_field_009='
      . $local_id;

    say "loading " . $sruUrl;
    my $ua       = LWP::UserAgent->new();
    my $response = $ua->get($sruUrl);
    my $sruResult =
      XML::LibXML->load_xml( string => $response->content )->documentElement;
    my $numberOfRecords =
      $sruResult->getChildrenByTagName('numberOfRecords')->to_literal;

    if ($numberOfRecords) {
        if ( $local_id =~ m{LJI} ) {
            $logger->warn("$local_id exists already in Alma!");
            return 'false';
        } else {
            $logger->warn("$local_id needs to be incremented.");
            $local_id =~ s{H}{I};
            $logger->info("New local id: $local_id");
            _check_local_id( $local_id, $logger, $conf );
        }
    } else {
        return $local_id;
    }
}

sub load_conf {
    my $conf_path =
        $test    ? '/opt/alma-tools/conf.yml'
      : $staging ? '/opt/alma-tools/config-staging/conf.yml'
      :            '/opt/alma-tools/config/conf.yml';
    return YAML::AppConfig->new( file => $conf_path );
}

sub initialize_logger {
### Initialize logger ###
    my $appender = Log::Log4perl::Appender->new(
        "Log::Dispatch::File",
        filename => "upload_thesis.log",
        mode     => "append",
    );
    my $errorscreen =
      Log::Log4perl::Appender->new( "Log::Dispatch::Screen", mode => "append",
      );

    #$errorscreen->threshold($ERROR);
    $errorscreen->threshold($INFO);
    my $layout =
      Log::Log4perl::Layout::PatternLayout->new("%d %p> %F{1}:%L %M - %m%n");
    $appender->layout($layout);
    $errorscreen->layout($layout);
    my $request_logger = get_logger("Alma::API::Client");
    my $logger         = get_logger();
    $request_logger->add_appender($appender);
    $request_logger->add_appender($errorscreen);
    $logger->add_appender($appender);
    $logger->add_appender($errorscreen);

    if ($verbose) {
        $request_logger->level($INFO);
        $logger->level($INFO);
    } elsif ($debug) {
        $request_logger->level($DEBUG);
        $logger->level($DEBUG);
    } else {
        $request_logger->level($WARN);
        $logger->level($WARN);
    }
### End logger initialization ###
    return $logger;
}

main();

