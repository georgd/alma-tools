#!/usr/bin/env perl

use strict;
use warnings;
use Path::Tiny;
use Data::Dumper qw( Dumper );
use YAML::AppConfig;
use MongoDB;
use Log::Log4perl qw( get_logger :levels );
use Getopt::Long qw( :config posix_default bundling no_ignore_case );
use POSIX qw(strftime);
use utf8::all;
use Alma::API::Analytics;

GetOptions(
    'test|t'    => \my $test,
    'verbose|v' => \my $verbose,
    'debug|d'   => \my $debug,
    'staging|s' => \my $staging,
);

my $conf_path =
  $staging
  ? '/opt/alma-tools/config-staging/conf.yml'
  : '/opt/alma-tools/config/conf.yml';
my $conf = YAML::AppConfig->new( file => $conf_path );

### Initialize logger ###
my $appender = Log::Log4perl::Appender->new(
    "Log::Dispatch::File",
    filename => "fetch_usage-data.log",
    mode     => "append",
);
my $errorscreen =
  Log::Log4perl::Appender->new( "Log::Dispatch::Screen", mode => "append", );

#$errorscreen->threshold($ERROR);
$errorscreen->threshold($INFO);
my $layout =
  Log::Log4perl::Layout::PatternLayout->new("%d %p> %F{1}:%L %M - %m%n");
$appender->layout($layout);
$errorscreen->layout($layout);
my $request_logger = get_logger("Alma::API::Client");
my $logger         = get_logger();
$request_logger->add_appender($appender);
$request_logger->add_appender($errorscreen);
$logger->add_appender($appender);
$logger->add_appender($errorscreen);

if ($verbose) {
    $request_logger->level($INFO);
    $logger->level($INFO);
} elsif ($debug) {
    $request_logger->level($DEBUG);
    $logger->level($DEBUG);
} else {
    $request_logger->level($WARN);
    $logger->level($WARN);
}
### End logger initialization ###

sub main {
    $logger->info('Starting main program');

    # get ebs-data:
    # read db ebs-helper db.ebsHelper.projects.find({ active => 1})
    my $projects = mongodb( 'ebsHelper', 'projects' );
    my @projects = $projects->find( { "active" => 1 } )->all;

    $logger->debug("Found " . scalar @projects . " projects");
    # foreach project find and sort runtimeMonths
    foreach my $project (@projects) {
        $logger->info("Working on $project->{name}");
        my @runtime_months = get_months($project);

        if ( !scalar @runtime_months ) {
            $logger->info("No open months in $project->{name}");
            next;
        }

        foreach my $month (@runtime_months) {
            my $data = fetch_report( $project, $month );

            if ( !scalar @$data ) {

                # report to logs/mail???
                $logger->warn('Empty report');
                next;
            }

            my $total_usage = parse_data( $data, $project, $month );
            my $output_path = 'runtimeMonths.' . $month . '.usage';
            #my $output_path = 'runtimeMonths.$.' . $month . '.usage';
            $logger->info("Writing monthly usage for $project->{name} to db.");
            $projects->update_one(
                { '_id'  => $project->{_id} },
                { '$set' => { $output_path => $total_usage } },
            );

        }
    }
}

sub get_months {
    my $project        = shift;
    my $current_month  = strftime "%Y-%m", localtime;
    my %runtime_months = %{ $project->{runtimeMonths} };
    my @keys           = sort { $a cmp $b } keys %runtime_months;
    my @months;
    foreach my $month (@keys) {

        # fetch data up to previous month
        if (   ( $month lt $current_month )
            && ( !$runtime_months{$month}{usage} ) )
        {
            push @months, $month;
        }
    }
    return @months;
}

sub fetch_report {
    my ( $project, $month ) = @_;

    my %api_params = (
        report_path =>
'/shared/WU Wirtschaftsuniversität Wien/Reports/E-Ressourcen/BR 2 Reports EBS',
        apikey => $conf->get_apikeys->{analytics},
    );

    my $platform = $project->{reportPlatform};

    my $filter =
qq{<sawx:expr xsi:type="sawx:logical" op="and" xmlns:saw="com.siebel.analytics.web/report/v1.1" xmlns:sawx="com.siebel.analytics.web/expression/v1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <sawx:expr xsi:type="sawx:comparison" op="equal">
               <sawx:expr xsi:type="sawx:sqlExpression">"Platform"."Platform"</sawx:expr>
               <sawx:expr xsi:type="xsd:string">$platform</sawx:expr></sawx:expr>
    <sawx:expr xsi:type="sawx:list" op="in">
               <sawx:expr xsi:type="sawx:sqlExpression">"Usage Date"."Usage Date"</sawx:expr>
               <sawx:expr xsi:type="xsd:date">$month-01</sawx:expr></sawx:expr>
</sawx:expr>};

    $api_params{filter} = $filter;

    my $report = Alma::API::Analytics->new( \%api_params );

    return $report->get_report();
}

sub parse_data {
    my ( $data, $project, $month ) = @_;

    my $total_usage = 0;

    foreach my $row (@$data) {

        # add new usage data to entry in projectCode.titleList

        # find an ISBN
        my $isbn;
        if ( $row->{'Normalized ISBN'} ) {
            $isbn = $row->{'Normalized ISBN'};
        } elsif ( $row->{'Normalized EISSN'} ) {
            $isbn = $row->{'Normalized EISSN'};
        } elsif ( $row->{'Normalized ISSN'} ) {
            $isbn = $row->{'Normalized ISSN'};
        } else {
            $isbn = $row->{'Title Identifier'};
            $isbn =~ s/(\d{13})/$1/;

            # might not work for all vendors
        }

        my $id;
        my $title_list = mongodb( $project->{'code'}, 'titleList' );
        if ( my $title = $title_list->find_one( { isbn => $isbn } ) ) {
            $id = $title->{_id};
        } elsif ( my $itle = $title_list->find_one( { pisbn => $isbn } ) ) {
            $id = $title->{_id};
        } else {
            $logger->warn(
                "$isbn – $row->{'Display Title'} not found in database!");
            next;
        }

        my $usage       = $row->{'BR2 - Book Success Section Requests (total)'};
        my $output_path = 'usage.' . $month;
        #my $output_path = 'usage.$.' . $month;

        # recalculate usage? (external program)
        $logger->info("Writing usage data for $id $row->{'Display Title'} to db.");
        $title_list->update_one(
            { '_id'  => $id },
            { '$set' => { $output_path => $usage } },
        );

        $total_usage += $usage;

    }

    path('usage_output.txt')->spew( Dumper($data) );
    return $total_usage;
}

sub mongoCollName {
    my $collName = join ".", @_;
    $collName =~ s/-/_/g;
    return $collName;
}

sub mongodb {
    my $collection = mongoCollName(@_);
    my $dbName     = $conf->get_app->{mongodb};
    my $dbUrl      = $conf->get_db_url;
    my $dbPort      = $conf->get_db_port;

    my $client = MongoDB::MongoClient->new( host => $dbUrl .":". $dbPort );
    my $db     = $client->get_database($dbName);
    return $db->get_collection($collection);
}

main();

