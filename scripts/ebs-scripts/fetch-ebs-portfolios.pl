#!/usr/bin/env perl

use strict;
use warnings;
use Path::Tiny;
use Data::Dumper qw( Dumper );
use YAML::AppConfig;
use MongoDB;
use Log::Log4perl qw( get_logger :levels );
use Getopt::Long qw( :config posix_default bundling no_ignore_case );
use Alma::API::Analytics;
use Alma::API::Acq::Funds;
use utf8::all;

GetOptions(
    'test|t'    => \my $test,
    'verbose|v' => \my $verbose,
    'debug|d'   => \my $debug,
    'staging|s' => \my $staging,
);

my $conf_path =
  $staging
  ? '/opt/alma-tools/config-staging/conf.yml'
  : '/opt/alma-tools/config/conf.yml';
my $conf = YAML::AppConfig->new( file => $conf_path );

### Initialize logger ###
my $appender = Log::Log4perl::Appender->new(
    "Log::Dispatch::File",
    filename => "fetch_portfolios.log",
    mode     => "append",
);
my $errorscreen =
  Log::Log4perl::Appender->new( "Log::Dispatch::Screen", mode => "append", );

#$errorscreen->threshold($ERROR);
$errorscreen->threshold($INFO);
my $layout =
  Log::Log4perl::Layout::PatternLayout->new("%d %p> %F{1}:%L %M - %m%n");
$appender->layout($layout);
$errorscreen->layout($layout);
my $request_logger = get_logger("Alma::API::Client");
my $logger         = get_logger();
$request_logger->add_appender($appender);
$request_logger->add_appender($errorscreen);
$logger->add_appender($appender);
$logger->add_appender($errorscreen);

if ($verbose) {
    $request_logger->level($INFO);
    $logger->level($INFO);
} elsif ($debug) {
    $request_logger->level($DEBUG);
    $logger->level($DEBUG);
} else {
    $request_logger->level($WARN);
    $logger->level($WARN);
}
### End logger initialization ###

sub main {
    $logger->info('Starting main program');

    my $newest_mod_date_file = $conf->get_portfolio_mod_date_file;
    my $newest_mod_date =
      -e $newest_mod_date_file
      ? path($newest_mod_date_file)->slurp
      : '1970-01-01';

    # read db ebs-helper db.ebsHelper.projects.find({ active => 1})
    my @projects = _mongodb( 'ebsHelper', 'projects' )->find( { "active" => 1 } )->all;

    foreach my $project(@projects){
        my %api_params = (
            report_path =>
    "/shared/WU Wirtschaftsuniversität Wien/Reports/E-Ressourcen/OUP-OSO-EBA-EB_Portfolios",
            apikey => $conf->get_apikeys->{analytics},
        );

        my $filter =
        qq{<sawx:expr xsi:type="sawx:logical" op="and" xmlns:saw="com.siebel.analytics.web/report/v1.1" xmlns:sawx="com.siebel.analytics.web/expression/v1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
        <sawx:expr xsi:type="sawx:list" op="containsAny">
        <sawx:expr xsi:type="sawx:sqlExpression">"Bibliographic Details"."Local Param 03"</sawx:expr>
        <sawx:expr xsi:type="xsd:string">$project->{code}</sawx:expr>
        </sawx:expr>
        <sawx:expr xsi:type="sawx:comparison" op="greater">
        <sawx:expr xsi:type="sawx:sqlExpression">"Portfolio Modification Date"."Portfolio Modification Date"</sawx:expr>
        <sawx:expr xsi:type="xsd:date">$newest_mod_date</sawx:expr>
        </sawx:expr>
        </sawx:expr>};

        $logger->debug($filter);

        $api_params{filter} = $filter;

        my $report = Alma::API::Analytics->new( \%api_params );

        my $data = $report->get_report();
        $logger->debug( sub {Dumper($data)} );

        if ( scalar @$data ) {
            parse_data( $data, $newest_mod_date, $newest_mod_date_file );
        } else {
            $logger->warn('Empty report');
        }
    }
}

sub parse_data {
    my ( $data, $new_newest_mod_date, $newest_mod_date_file ) = @_;

    my %funds;

    foreach my $row (@$data) {
        my %output;

        # # split ISBNs to e and print
        # my @ISBNs = split '; ', $row->{'ISBN (Normalized)'};
        # foreach my $isbn (@ISBNs) {
        #     next unless $isbn =~ m/\d{13}/;
        #     if ( $output{isbn} ) {
        #         $output{pisbn} = $isbn;
        #     } else {
        #         $output{isbn} = $isbn;
        #     }
        # }

        # rename local param 03 and retain only package date
        my ( $ebs_project, $package_month ) = split "; ",
          $row->{'Local Param 03'};
        $ebs_project   =~ s/^\s*\$\$a\s*//;
        $package_month =~ s/^\s*\$\$b\s*//;
        $output{packageMonth} = $package_month;

        # rename MMS Id
        $output{mmsId} = $row->{'MMS Id'};

        # split Network Number
        $output{localId} = $row->{'Network Number'};
        $output{localId} =~ s/.*(LJ\d{8}).*/$1/;

        # rename Originating System ID
        my $id = $row->{'Originating System ID'};

        # add publication year
        $output{year} = $row->{'Publication Date'};
        $output{year} =~ s/.*?(\d{4}).*/$1/;

        # rename Portfolio Activation Date
        $output{portfolioActivationDate} = $row->{'Portfolio Activation Date'};

        # Rename Portfolio Modification Date and calculate newest mod
        my $mod_date = $row->{'Portfolio Modification Date'};
        $output{portfolioModificationDate} = $mod_date;
        if ( $mod_date gt $new_newest_mod_date ) {
            $new_newest_mod_date = $mod_date;
        }

        # Rename Portfolio Id
        $output{portfolioId} = $row->{'Portfolio Id'};

        # Rename Portfolio Internal Description
        if ( $row->{'Portfolio Internal Description'} ) {
            my $d = $row->{'Portfolio Internal Description'};
            $output{portfolioIntDesc} = $d;
            if ( $d ne 'PRINT' ) {
                $funds{$ebs_project}{$d} = 1;
            }
        }

        # throw away 0, Originating System, Availability, Portfolio Linked to CZ
        #
        $logger->debug("Updating title $id in project $ebs_project");
        my $portfolioTable = _mongodb( $ebs_project, 'titleList' );

        my $res = $portfolioTable->update_one(
            { '_id'  => $id },
            { '$set' => \%output },
        );
        $logger->debug( Dumper($res) );
    }

    if (%funds) {
        add_new_funds(%funds);
    }

    path('oup_output.txt')->spew( Dumper($data) );

    path($newest_mod_date_file)->spew($new_newest_mod_date);
}

sub add_new_funds {
    my %funds = @_;

    foreach my $project ( keys %funds ) {
        my $projects = _mongodb( 'ebsHelper', 'projects' );
        my $p = $projects->find_one( { 'code' => $project } );
        foreach my $fund ( keys %{ $funds{$project} } ) {
            next
              if $p->{funds}{$fund}
              && $p->{funds}{$fund} ne 'no fund with this code';

            my %api_params = (
                fund_code => $fund,
                apikey    => $conf->get_apikeys->{acq_rw},
                id        => $fund,
            );
            my $f = Alma::API::Acq::Funds->new( \%api_params );
            my $name =
              $f->get()->{content}->{fund}->[0]->{name}
              || 'no fund with this code';

            my $res = $projects->update_one( { 'code' => $project },
                { '$set' => { "funds.$fund" => $name } } );
            $logger->info( Dumper($res) );
        }
    }
}

sub _mongoCollName {
    my $collName = join ".", @_;
    $collName =~ s/-/_/g;
    return $collName;
}

sub _mongodb {
    my $collection = _mongoCollName(@_);
    my $dbName     = $conf->get_app->{mongodb};
    my $dbUrl      = $conf->get_db_url;
    my $dbPort     = $conf->get_db_port;

    my $client = MongoDB::MongoClient->new( host => $dbUrl . ":" . $dbPort );
    my $db     = $client->get_database($dbName);
    return $db->get_collection($collection);
}

main();
