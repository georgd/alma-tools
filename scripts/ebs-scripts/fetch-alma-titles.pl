#!/usr/bin/env perl

use v5.16;
use strict;
use warnings;
use Path::Tiny;
use Data::Dumper qw( Dumper );
use YAML::AppConfig;
use MongoDB;
use Log::Log4perl qw( get_logger :levels );
use Getopt::Long qw( :config posix_default bundling no_ignore_case );
use Alma::API::Analytics;
use Alma::API::Acq::Funds;
use utf8::all;
use POSIX qw(strftime);

use LWP;
use XML::LibXML;

GetOptions(
    'test|t'    => \my $test,
    'verbose|v' => \my $verbose,
    'debug|d'   => \my $debug,
    'staging|s' => \my $staging,
);

my $conf_path =
  $staging
  ? '/opt/alma-tools/config-staging/conf.yml'
  : '/opt/alma-tools/config/conf.yml';
my $conf = YAML::AppConfig->new( file => $conf_path );

### Initialize logger ###
my $appender = Log::Log4perl::Appender->new(
    "Log::Dispatch::File",
    filename => "fetch_portfolios.log",
    mode     => "append",
);
my $errorscreen =
  Log::Log4perl::Appender->new( "Log::Dispatch::Screen", mode => "append", );

#$errorscreen->threshold($ERROR);
$errorscreen->threshold($INFO);
my $layout =
  Log::Log4perl::Layout::PatternLayout->new("%d %p> %F{1}:%L %M - %m%n");
$appender->layout($layout);
$errorscreen->layout($layout);
my $request_logger = get_logger("Alma::API::Client");
my $logger         = get_logger();
$request_logger->add_appender($appender);
$request_logger->add_appender($errorscreen);
$logger->add_appender($appender);
$logger->add_appender($errorscreen);

if ($verbose) {
    $request_logger->level($INFO);
    $logger->level($INFO);
} elsif ($debug) {
    $request_logger->level($DEBUG);
    $logger->level($DEBUG);
} else {
    $request_logger->level($WARN);
    $logger->level($WARN);
}
### End logger initialization ###

sub main {
    $logger->info('Starting main program');

    my $timestamp = _createTimestamp();
    my $titleTable = _mongodb( 'VFV-VEL-EBA-EB', 'titleList' );

    my $sruUrl =
    #$conf->get_alma->{domain}
        'https://obv-at-ubww.alma.exlibrisgroup.com'
      . '/view/sru/'
      . $conf->get_alma->{inst_code}
      . '?version=1.2&operation=searchRetrieve&recordSchema=marcxml'
      . '&query=alma.local_field_990=VFV-VEL-EBA-EB'
      . '%20and%20alma.mms_createDate>2020-04-09';

    say "loading " . $sruUrl;
    my $ua       = LWP::UserAgent->new();
    my $response = $ua->get($sruUrl);
    my $sruResult =
      XML::LibXML->load_xml( string => $response->content )->documentElement;

    my $numberOfRecords =
      $sruResult->getChildrenByTagName('numberOfRecords')->to_literal;

    my $xpc = XML::LibXML::XPathContext->new($sruResult);
    $xpc->registerNs( sru => 'http://www.loc.gov/zing/srw/' );
    $xpc->registerNs( marc => 'http://www.loc.gov/MARC21/slim' );
    foreach my $record($xpc->findnodes('//sru:recordData/marc:record')){
        my $xpcm = XML::LibXML::XPathContext->new($record);
        $xpcm->registerNs( marc => 'http://www.loc.gov/MARC21/slim' );
        my %rec;
        $rec{mmsId} = $xpcm->findvalue('./marc:controlfield[@tag="001"]');
        $rec{_id} = $rec{mmsId};
        # my $res = $titleTable->delete_one({'_id' => $rec{_id}});
        $rec{title} = join " : ", map { 
            $_->to_literal(); 
            }
            $xpcm->findnodes('./marc:datafield[@tag="245"]/marc:subfield[@code="a" or @code="b"]');
        $rec{author} = join ' ; ', map {
            $_->to_literal();
            }
            $xpcm->findnodes('./marc:datafield[@tag="100" or @tag="700"]/marc:subfield[@code="a"]');
        $rec{doi} = $xpcm->findvalue('./marc:datafield[@tag="024"]/marc:subfield[@code="a"]');
        $rec{isbn} = $xpcm->findvalue('./marc:datafield[@tag="020"]/marc:subfield[@code="a"]');
        $rec{pisbn} = $xpcm->findvalue('./marc:datafield[@tag="776"]/marc:subfield[@code="z"]');
        $rec{year} = $xpcm->findvalue('./marc:datafield[@tag="264"]/marc:subfield[@code="c"]');
        $rec{sigel} = $xpcm->findvalue(
            './marc:datafield[@tag="912"]/marc:subfield[@code="a" 
                and not(contains(text(), "ZDB-128-VEL")) 
                and not(contains(text(), "ZDB-128-BJZ2"))]');
        $rec{sigel} =~ s/ZDB-18-BEL// if length $rec{sigel} > 18;
        $rec{packageMonth} = $xpcm->findvalue('./marc:datafield[@tag="990"]/marc:subfield[@code="b"]');
        $rec{localId} = $xpcm->findvalue('./marc:datafield[@tag="035"]/marc:subfield[@code="a" and text()[contains(.,"(VFV)")]]');
        $rec{timestamp} = $timestamp;
        $logger->debug( Dumper(%rec) );

        my $res = $titleTable->insert_one(
            \%rec
        );

    }
      $logger->info( "Number of records: " . $numberOfRecords );

}


sub _mongoCollName {
    my $collName = join ".", @_;
    $collName =~ s/-/_/g;
    return $collName;
}

sub _mongodb {
    my $collection = _mongoCollName(@_);
    my $dbName     = $conf->get_app->{mongodb};
    my $dbUrl      = $conf->get_db_url;
    my $dbPort     = $conf->get_db_port;

    my $client = MongoDB::MongoClient->new( host => $dbUrl . ":" . $dbPort );
    my $db     = $client->get_database($dbName);
    return $db->get_collection($collection);
}

sub _createTimestamp {
    return strftime "%y%m%d-%H%M%S", localtime;
}

main();

