#!/usr/bin/env perl
# Add local IDs to a list of Alma records

use Modern::Perl '2016';
use Data::Dumper qw( Dumper );
use Path::Tiny;
use YAML::AppConfig;
use Log::Log4perl qw( get_logger :levels );
use Getopt::Long qw( :config posix_default bundling no_ignore_case );
use XML::LibXML;
use Alma::API::Bibs;

GetOptions(
    'test|t'    => \my $test,
    'staging|s' => \my $staging,
    'input=s'   => \my $inputfile,
    'verbose|v' => \my $verbose,
    'debug|d'   => \my $debug,
);

my $conf   = load_conf();
my $logger = initialize_logger();

my $localID = 1000;
if ( -e 'LJNummer.txt' ) {
    $localID = path('LJNummer.txt')->slurp;
}

sub main {
    $logger->info('Starting main program');

    my @mmsIDs = path($inputfile)->lines;

    foreach my $mmsID (@mmsIDs) {
        next unless $mmsID =~ m/9\d+3337/;

        my $dom = XML::LibXML->load_xml( string => getBib($mmsID) );

        if ( my $output = verifyAndModifyBib( $dom, $mmsID ) ) {
            say $output;
            if ( putBib( $output, $mmsID ) eq 'SUCCESS!' ) {
                $localID++;
            }
        }
    }
    writeLocalID();
}

sub getBib {
    my $mms = shift;
    chomp $mms;
    my $api_call = Alma::API::Bibs->new(
        apikey => $conf->get_apikeys->{bibs_rw},
        id     => $mms,
        mms_id => $mms
    );
    return $api_call->get()->{content};
}

sub verifyAndModifyBib {
    my $dom = shift;
    my $id  = shift;

    # Don't overwrite existing 009
    if ( $dom->findnodes('//controlfield[@tag="009"]') ) {
        $logger->warn("$id already has a 009!");
        return 0;
    }

    # Don't modify linked records
    if ( $dom->findvalue('linked_record_id') ) {
        $logger->warn("$id seems to be linked to NZ or CZ.");
        return 0;
    }

    # Test if local ID hasn't been assigned yet
    verifyLocalID();

    # Locate 008 to insert 009 after it
    my ($field008) = $dom->findnodes('//controlfield[@tag="008"]');
    print $field008;

    # create 009
    my $LJnumber = sprintf "LJ%08d", $localID;
    $logger->info("Create field 009 with $localID for $id.");
    my $field009 = $dom->createElement('controlfield');
    $field009->appendText($LJnumber);
    $field009->{tag} = '009';
    $field008->parentNode->insertAfter( $field009, $field008 );

    # create 035
    my $LJsystemNumber = '(AT-UBWW)' . $LJnumber;
    my ($old035)       = $dom->findnodes('//datafield[@tag="035"]');
    my $field035       = $dom->createElement('datafield');
    $field035->{ind1} = ' ';
    $field035->{ind2} = ' ';
    $field035->{tag}  = '035';
    my $subfield035 = $dom->createElement('subfield');
    $subfield035->appendText($LJsystemNumber);
    $subfield035->{code} = 'a';
    $field035->addChild($subfield035);
    $old035->parentNode->insertBefore( $field035, $old035 );

    $localID++;

    return ( $dom->toString );
}

sub putBib {
    my $output = shift;
    my $mms    = shift;
    chomp $mms;
    my $api_call = Alma::API::Bibs->new(
        apikey => $conf->get_apikeys->{bibs_rw},
        id     => $mms,
        mms_id => $mms
    );
    return $api_call->put($output) unless $test;
}

sub verifyLocalID {
    my $lj = sprintf "LJ%08d", $localID;
    my $sruUrl =
        'http://obv-at-ubww.alma.exlibrisgroup.com/view/sru/43ACC_WUW?'
      . 'version=1.2&operation=searchRetrieve&'
      . 'query=alma.local_control_field_009='
      . $lj;

    my $sruResult = XML::LibXML->load_xml( location => $sruUrl );
    my $xpc = XML::LibXML::XPathContext->new;
    $xpc->registerNs('s', 'http://www.loc.gov/zing/srw/');
    my ($numberOfRecords) = $xpc->findvalue('//s:numberOfRecords', $sruResult);
    if ($numberOfRecords) {
        $logger->warn("$localID was already assigned.");
        $localID++;
        verifyLocalID();
    }
}

sub writeLocalID {
    path('LJNummer.txt')->spew($localID);
}

sub load_conf {
    my $conf_path =
        $test    ? '/opt/alma-tools/conf.yml'
      : $staging ? '/opt/alma-tools/config-staging/conf.yml'
      :            '/opt/alma-tools/config/conf.yml';
    return YAML::AppConfig->new( file => $conf_path );
}

sub initialize_logger {
### Initialize logger ###
    my $appender = Log::Log4perl::Appender->new(
        "Log::Dispatch::File",
        filename => "createLocalID.log",
        mode     => "append",
    );
    my $errorscreen =
      Log::Log4perl::Appender->new( "Log::Dispatch::Screen", mode => "append",
      );

    #$errorscreen->threshold($ERROR);
    $errorscreen->threshold($INFO);
    my $layout =
      Log::Log4perl::Layout::PatternLayout->new("%d %p> %F{1}:%L %M - %m%n");
    $appender->layout($layout);
    $errorscreen->layout($layout);
    my $request_logger = get_logger("Alma::API::Client");
    my $logger         = get_logger();
    $request_logger->add_appender($appender);
    $request_logger->add_appender($errorscreen);
    $logger->add_appender($appender);
    $logger->add_appender($errorscreen);

    if ($verbose) {
        $request_logger->level($INFO);
        $logger->level($INFO);
    } elsif ($debug) {
        $request_logger->level($DEBUG);
        $logger->level($DEBUG);
    } else {
        $request_logger->level($WARN);
        $logger->level($WARN);
    }
### End logger initialization ###
    return $logger;
}

main();

