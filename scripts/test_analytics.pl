use strict;
use warnings;
use Path::Tiny;
use Data::Dumper qw( Dumper );
use Log::Log4perl qw( get_logger :levels );
use Getopt::Long qw( :config posix_default bundling no_ignore_case );
use Alma::API::Analytics;

GetOptions(
    'test|t'    => \my $test,
    'verbose|v' => \my $verbose,
    'debug|d'   => \my $debug,
    'apikey'    => \my $apikey,
);

### Initialize logger ###
my $appender = Log::Log4perl::Appender->new(
    "Log::Dispatch::File",
    filename => "test_analytics.log",
    mode     => "append",
);
my $errorscreen =
  Log::Log4perl::Appender->new( "Log::Dispatch::Screen", mode => "append", );

#$errorscreen->threshold($ERROR);
$errorscreen->threshold($INFO);
my $layout =
  Log::Log4perl::Layout::PatternLayout->new("%d %p> %F{1}:%L %M - %m%n");
$appender->layout($layout);
$errorscreen->layout($layout);
my $request_logger = get_logger("Alma::API::Client");
my $logger         = get_logger();
$request_logger->add_appender($appender);
$request_logger->add_appender($errorscreen);
$logger->add_appender($appender);
$logger->add_appender($errorscreen);

if ($verbose) {
    $request_logger->level($INFO);
    $logger->level($INFO);
} elsif ($debug) {
    $request_logger->level($DEBUG);
    $logger->level($DEBUG);
} else {
    $request_logger->level($WARN);
    $logger->level($WARN);
}
### End logger initialization ###

$logger->info('Starting main program');

my $path =
'/shared/WU Wirtschaftsuniversität Wien/Reports/E-Ressourcen/OUP-OSO-EBA-EB_Portfolios';

#my $path = '%2Fshared%2FWU+Wirtschaftsuniversit%C3%A4t+Wien%2FReports%2FE-Ressourcen%2FOUP-OSO-EBA-EB_Portfolios';

my $report = Alma::API::Analytics->new(
    path   => $path,
    apikey => $apikey,
);

my $data = $report->get_report();

# my @output;
# foreach my $row (@$data){
#     push @output, join "\t", @$row;
# }
#
# path('output.csv')->write_lines(@output);
path('output.csv')->spew( Dumper($data) );
