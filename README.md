# Alma Tools

Various Perl libraries and scripts for use with Alma.

The libs contain packages for various Alma APIs, additional ones will be added when needed.

The Analytics API library is mostly derived from the Alma2 Analytics Perl Library by Curtin University library (https://bitbucket.org/curtin-library/almaanalytics/src/master/).


