
# This file is part of the Alma2 Analytics Perl Library.
#
# Alma2 Analytics Perl Library is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Alma2 Analytics Perl Library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Alma2 Analytics Perl Library.  If not, see <http://www.gnu.org/licenses/>.
#
# package Alma2::XPC;

package Alma::API::Analytics::XPC;

use strict;
use warnings;
use Data::Dumper;

use XML::LibXML;

# ========================================================================================================================

sub new {
	my ($class, $xpc) = @_;
	my $self = {
		xpc => $xpc,
	};
	return bless $self, $class;
}

# ========================================================================================================================

sub xpc {
	return $_[0]->{xpc};
}

sub find_arrayref {
	my ($self, $dom, $path) = @_;
	my $xpc = $self->{xpc};
	my @doms = $xpc->findnodes($path, $dom);
	return \@doms;
}

sub find_firstref {
	my ($self, $dom, $path) = @_;
	my $xpc = $self->{xpc};
	my @nodes = $xpc->findnodes($path, $dom);
	return (@nodes > 0 ? $nodes[0] : undef);
}

sub get_text {
	my ($self, $dom, $path) = @_;
	my $xpc = $self->{xpc};
	my ($node) = $xpc->findnodes($path, $dom);
	return (defined $node ? ($node->textContent() || '') : '');
}

# ========================================================================================================================

sub is_xml {
	my ($raw) = @_;

	# 55 = length of xml pre-amble <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
	my $s = substr($raw, 0, 55);
	$s =~ s/^\s*//g;

	return ($s =~ /^<\?xml/i ? 1 : 0);
}

# ========================================================================================================================

1;

