#
# This file uses parts of the Alma2 Analytics Perl Library by Curtin University library.
#
# This file is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
package Alma::API::Analytics;

use Modern::Perl '2016';
use utf8::all;

# use Data::Dumper qw( Dumper );
# use Path::Tiny;
use Log::Log4perl qw( :easy );
use XML::LibXML;

use Moose;

use Alma::API::Client;
use Alma::API::Analytics::XPC;

has [ 'report_path', 'apikey' ] => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has '_token' => (
    is        => 'rw',
    isa       => 'Str',
    predicate => 'has_token',
);

has '_more' => (
    traits  => ['Bool'],
    is      => 'rw',
    isa     => 'Bool',
    default => 1,
    handles => { no_more => 'unset', },
);

has 'skipColumn0' => (
    traits  => ['Bool'],
    is      => 'rw',
    isa     => 'Bool',
    default => 1,
    handles => { not_skipColumn0 => 'unset', },
);

has 'filter' => (
    is        => 'ro',
    isa       => 'Str',
    predicate => 'has_filter',
);

has '_counter_requests' => (
    traits  => ['Counter'],
    is      => 'ro',
    isa     => 'Num',
    default => 0,
    handles => { _inc_counter_requests => 'inc' },
);

has 'param_col_names' => (
    is      => 'rw',
    isa     => 'Str',
    default => 'true',
);

has '_row_element' => (
    is        => 'rw',
    isa       => 'Str',
    predicate => 'has_row_element',
);

has '_lutcols' => (
    traits  => ['Hash'],
    is      => 'rw',
    isa     => 'HashRef',
    default => sub { {} },
    handles => {
        set_lutcols => 'set',
        get_lutcols => 'get',
    },
);

has '_column_defs' => (
    traits  => ['Array'],
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub { [] },
    handles => {
        all_column_defs => 'elements',
        add_column_defs => 'push',
        map_column_defs => 'map',
    },
);

has 'rows' => (
    traits  => ['Array'],
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub { [] },
    handles => {
        add_rows => 'push',
    }
);

has 'log' => (
    is => 'ro',
    isa => 'Log::Log4perl::Logger',
    default => sub {get_logger("Alma::API::Analytics")},
    lazy => 1,
);

sub get_report {
    my $self   = shift;

    while ( $self->_more ) {
        my $response = $self->fetch_report();
        $self->parse_response($response);
    }

    return $self->rows;
}

sub fetch_report {
    my $self = shift;

    my $params->{limit} = 1000;
    $params->{col_names} = $self->param_col_names;
    if ( $self->has_token ) {
        $params->{token} = $self->_token;
    } else {
        $params->{path}   = $self->report_path;
        $params->{filter} = $self->filter if $self->has_filter;
    }

    $self->_inc_counter_requests;

    my $request = Alma::API::Client->new(
        apikey         => $self->apikey,
        params         => $params,
        service        => 'analytics/reports',
        'content_type' => 'application/xml',
        id             => 'Request no. ' . $self->_counter_requests,
    );

    my $response = $request->get()->{content};
    print 'Get response: ', $response;

    return $response;
}

sub parse_response {
    my ( $self, $xml ) = @_;

    # parse the header
    my $dom   = XML::LibXML->load_xml( string => $xml );
    my $xpath = XML::LibXML::XPathContext->new();
    $xpath->registerNs( 'x', 'urn:schemas-microsoft-com:xml-analysis:rowset' );
    $xpath->registerNs( 'xsd', 'http://www.w3.org/2001/XMLSchema' );
    my $xpc = Alma::API::Analytics::XPC->new($xpath);

    # resumption token!
    my $nodes = $xpc->find_arrayref( $dom, '//QueryResult/ResumptionToken' );
    if ( scalar @$nodes ) {
        $self->_token( $nodes->[0]->textContent );
    }

    # end of query?
    $nodes = $xpc->find_arrayref( $dom, '//QueryResult/IsFinished' );
    if ( scalar @$nodes ) {
        $self->no_more if $nodes->[0]->textContent eq 'true';
    }

   # row-element and column definitions. these will only occur once and together
   # (in the first block returned by the api)
    $nodes =
      $xpc->find_arrayref( $dom, '//QueryResult/ResultXml//xsd:complexType' );
    if ( scalar @$nodes ) {

        # get the XML tagname of the row element
        my $node = $nodes->[0];
        $self->_row_element( $node->getAttribute('name') );

        # save the column definitions
        $nodes = $xpc->find_arrayref( $node, 'xsd:sequence/xsd:element' );
        if ( scalar @$nodes ) {
            my $x       = 0;
            my @columns = map {
                {
                    name => $_->getAttribute("name"),
                    heading =>
                      $_->getAttributeNS( "urn:saw-sql", "columnHeading" ),
                    type => $_->getAttributeNS( "urn:saw-sql", "type" ),
                    pos  => $x++,
                }
            } @$nodes;
            if ( $columns[0]->{heading} ) {
                $self->not_skipColumn0;
            }
            $x = 0;
            my %lutcols = map { $_->{name} => $x++, } @columns;
            $self->_column_defs( \@columns );
            $self->_lutcols( \%lutcols );
        }
    }

    # if $row_element is undefined at this point then something isn't right
    # with the XML
    return unless $self->has_row_element;

    # ========================================
    # parse the body.
    # by default, oracle analytics does not return any column which is null,
    # so special checking is needed to skip these empty columns

    my $rows = $xpc->find_arrayref( $dom,
        "/report/QueryResult/ResultXml/x:rowset/x:" . $self->_row_element );
    foreach my $row (@$rows) {
        my %row;
        foreach my $coldef ( $self->all_column_defs ) {
            my $heading = $coldef->{heading};
            my $colname = $coldef->{name};
            my $type    = $coldef->{type};
            my $x       = $self->get_lutcols($colname);
            $row{$heading} = default_value($type);

            my @col = $row->getChildrenByTagNameNS( '*', $colname );
            next if !@col;

            $row{$heading} = $col[0]->textContent;
        }

        #shift @row if $self->skipColumn0;

        #$row = $rowtmpl->toStruct(\@row);
        $self->add_rows( \%row );
    }

    return;
}

sub default_value {
    my ($type) = @_;
    return 0 if $type eq 'integer' or $type eq 'number' or $type eq 'double';
    return ''                       if $type eq 'varchar';
    return '0000-00-00'             if $type eq 'date';
    return '0000-00-00 00:00:00'    if $type eq 'datetime';
    return '0000-00-00 00:00:00.00' if $type eq 'timestamp';
    return '';
}

1;

