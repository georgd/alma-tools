# v. 0.1
package  Alma::API::Client;

use Modern::Perl '2016';
use utf8::all;
use Data::Dumper qw( Dumper );
use IO::Socket::SSL;
use LWP::UserAgent;
use HTTP::Request;
use JSON::MaybeXS;
use Encode qw(encode_utf8);
use Carp;
use Log::Log4perl qw( :easy );

use Moose;

has [ 'service', 'apikey' ] => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has 'url' => (
    is      => 'ro',
    isa     => 'Str',
    default => 'https://api-eu.hosted.exlibrisgroup.com/almaws/v1/',
);

has 'content_type' => (
    is      => 'ro',
    isa     => 'Str',
    default => 'application/json',
);

has 'id' => (
    is  => 'ro',
    isa => 'Any',
);

has 'params' => (
    is        => 'ro',
    isa       => 'HashRef',
    predicate => 'has_params',
);

has 'log' => (
    is => 'ro',
    isa => 'Log::Log4perl::Logger',
    default => sub {get_logger("Alma::API::Client")},
    lazy => 1,
);

sub get {
    my $self = shift;

    #my $input  = $_[0] ? shift : '{parameter: []}';
    if (my $input = shift){
        return $self->request('GET', $input);
    }

    return $self->request( 'GET' );
}

sub put {
    my $self = shift;

    my $input  = shift
      or $self->log->logcroak( 'Missing input in ', $self->id, '!' );

    return $self->request( 'PUT', $input );
}

sub post {
    my $self = shift;

    my $input  = shift
      or $self->log->logcroak( 'Missing input in ', $self->id, '!' );

    return $self->request( 'POST', $input );
}

sub delete {
    my $self = shift;
    return $self->request('DELETE');
}

sub request {
    my $self = shift;

    my ( $method, $input ) = @_;

    $self->log->debug( "$method ", $self->id, '.' );

    my $uri = URI->new( $self->url . $self->service );
    if ( $self->has_params ) {
        $uri->query_form( $self->params );
    }

    my $h = [
        Content_type  => $self->content_type,
        Accept        => $self->content_type,
        Authorization => 'apikey ' . $self->apikey,
    ];

    push my @request_arguments, ( $method, $uri, $h );

    if ($input) {
        if ( $self->content_type eq 'application/json' ) {
            $input = encode_json($input);
        }
        push @request_arguments, encode_utf8($input);
    }

    my $request = HTTP::Request->new(@request_arguments);

    $self->log->debug( $request->as_string );

    my $ua       = LWP::UserAgent->new;
    my $response = $ua->request($request);

    $self->log->debug( sub { Dumper($response) } );
    if ( $response->is_success ) {
        $self->log->info( "$method ", $self->id, ' successfull.' );
    } else {
        $self->log->logcarp( "HTTP $method error code: ",    $response->code );
        $self->log->logcarp( "HTTP $method error message: ", $response->message );
    }

    my $content = $response->content;
    if ($self->content_type eq 'application/json'){
        $content = decode_json($response->content);
    }
    return { msg => $response->message, content => $content };
}

1;
