package ALMA::User;

use Modern::Perl '2016';
use utf8::all;
use Data::Dumper qw( Dumper );
use Carp;
use Log::Log4perl qw( :easy );

use Moose;
use Moose::Util::TypeConstraints;
#use MooseX::UndefTolerant;
use ALMA::Types ':all';
#use Moose::Meta::Attribute;

# has 'record_type' => (
#   is  => 'ro',
#   isa => 'Str',      # STAFF, PUBLIC or CONTACT
# );

# subtype 'ALMARoles',
#   as 'HashRef';
#
# coerce 'ALMARoles',
#   from 'ArrayRef',
#   via  { _build_roles($_) };

my %type_by_value = (
  'DEFAULT_CIRC_DESK' => 'CirculationDesk',
  'DEFAULT_CIRC_DESK-ADJ.SPB' => 'CirculationDesk',
  'LC.Admin'          => 'CirculationDesk',
  'LC.CLD'            => 'CirculationDesk',
  'LC.Hand'           => 'CirculationDesk',
  'LC.Magazin'        => 'CirculationDesk',
  'AcqDeptJHB'        => 'ServiceUnit',
  'AcqDeptJSLZ'       => 'ServiceUnit',
  'WUW.ADJ'           => 'ServiceUnit',
  'WUW.BUBI'          => 'ServiceUnit',
  'WUW.ZS'            => 'ServiceUnit',
  );

has 'primary_id' => (
  is     => 'rw',
  isa    => 'Str',
  #writer => 'change_primary_id',
);

has [ 'first_name', 'middle_name', 'last_name', 'password' ] => (
  is  => 'ro',
  isa => 'Str',
);

has [ 'purge_date', 'expiry_date', 'birth_date' ] => (
  is  => 'ro',
  isa => 'Str',     # Date!!!
);

has [ 'preferred_language', 'cataloger_level', 'job_category', 'user_title', 'record_type', 'gender', 'user_group', 'account_type', 'status' ] => (
  is  => 'ro',
  isa => AlmaValue,
  coerce => 1,
  #isa => 'HashRef',      # AlmaValue+desc
  #builder => '_buildAlmaValueFromHash',
);

has 'campus_code' => (
  is     => 'ro',
  isa    => AlmaValue,
  coerce => 1,
);

has 'contact_info' => (
  is  => 'ro',
  isa => 'HashRef',   # Hash of arrays of addresses and phone nrs
);

has [ 'user_statistic', 'user_role', 'user_note', 'user_block' ] => (
  is  => 'ro',
  isa => 'ArrayRef',
);

# has 'new_roles' => (
#   is  => 'ro',
#   isa => 'ALMARoles',
#   init_arg => 'user_role',
# );

has 'user_role' => (
  is  => 'rw',
  # isa => 'ALMARoles',
  isa => 'HashRef',
  init_arg => 'orig_role',
);

has [ 'job_description', 'web_site_url', 'link' ] => (
  is  => 'ro',
  isa => 'Str',
);

has [ 'loans', 'requests' ] => (
  is      => 'ro',
  isa     => 'Int',    # Int
  # lazy    => 1,
  # default => undef,
);

has 'fees' => (
  is      => 'ro',
  isa     => 'Num',     # float!
  # lazy    => 1,
  # default => undef,
);

has 'user_identifier' => (
  is  =>    'rw',
  isa =>    'ArrayRef',     # Identifier!
  # writer => 'change_user_identifier',
);

has 'external_id' => (
  is  => 'ro',
  isa => 'Str',
);

around BUILDARGS => sub {
  #say 'Dump @_ :', Dumper(@_);
  my $orig = shift;
  my $class = shift;
  my $hash = shift;
  foreach my $key ( keys %$hash ){
    unless ( defined $hash->{$key} ){
      delete $hash->{$key};
      say "deleted $key";
      next;
    }
    if ( $key eq 'user_role' ){
      foreach my $role ( @{ $hash->{$key} } ){
        # say Dumper($role);
        my $roletype = $role->{role_type}{value};
        my $scope = $role->{scope}{value};
        my $status = $role->{status}{value} eq 'ACTIVE' ? 'a' : 'i';
        my @params = @{ $role->{parameter} };
        my $role_key = join "_", $roletype, $scope;
        # say $role_key;
        # my $user_role = ALMA::Role->new( %$role );
        # $hash->{orig_role}{$role_key}{data} = $user_role;
        $hash->{orig_role}{$role_key}{data} = $role;
        foreach my $param ( @params ){
          $hash->{orig_role}{$role_key}{params}{$param} = 1;
        }
      }
    }
  }
  return $hash;
};

sub _build_roles {
  my @roles = @$_;
  foreach my $role ( @roles ){
    say Dumper( $role );
    ...
  }
}

sub build_output {
  my $self = shift;
  my $meta = $self->meta;
  my %output;
  for my $attribute ( map { $meta->get_attribute($_) }
         sort $meta->get_attribute_list ) {
    my $name = $attribute->name;
    next unless $self->$name;
    my $val;
    if ( $name eq 'user_role' ){
      foreach my $role ( keys %{ $self->{$name} } ){
        # my $role_obj = $self->{$name}{$role}{data}->read_object();
        # push @{ $output{$name} }, $role_obj;
        push @{ $output{$name} }, $self->{$name}{$role}{data};
      }
    } elsif ( $attribute->type_constraint == AlmaValue ) {
      $output{$name} = { value => $self->$name };
    } else {
      $output{$name} = $self->$name;
    }
  }
  return \%output;
}

sub add_role {
  my $self = shift;
  my $role = shift;
  my $logger = get_logger( "ALMA::RequestAPI" );
  $logger->debug( "Add role from string ", $role, "." );
  my ( $type, $scope, $status, @parameter ) = split ",", $role;
  my $role_key = join "_", $type, $scope;
  $logger->debug( "Built role-key ", $role_key, "." );
  # Setting correct status.
  $status = $status =~ m/^i/i
                    ? 'INACTIVE'
                    : 'ACTIVE';
  # check if role exists for scope and if has same status
  if ( $self->{user_role}{$role_key}
    && $self->{user_role}{$role_key}{data}{status}{value} eq $status ){
    $logger->info( "Found role ", $role_key, "." );
    # check if parameters exist in new role
    if ( @parameter ){
      foreach my $param ( @parameter ){
        # check if parameters exist already
        unless ( $self->{user_role}{$role_key}{params}{$param} ){
          push @{ $self->{user_role}{$role_key}{data}{parameter} },
            { value => { value => $param },
              type  => { value => $type_by_value{$param} }
            };
          $self->{user_role}{$role_key}{params}{$param} = 1;
          $logger->info( "Pushed param ", $param, " to existing role ", $role_key, "." );
        }
      }
    }
  } else {
    $self->{user_role}{$role_key}{data} = {
      role_type => { value => $type },
      scope     => { value => $scope },
      status    => { value => $status },
    };
    $logger->info( "Built new role ", $role_key, "." );
    #say Dumper($self->{user_role}{$role_key}{data});
    if ( @parameter ){
      foreach my $param ( @parameter ){
        push @{ $self->{user_role}{$role_key}{data}{parameter} },
          { value => { value => $param },
            type  => { value => $type_by_value{$param} },
          };
        $logger->info( "Pushed param ", $param, " to role ", $role_key, ".");
      }
    }
  }
}

sub change_primary_id {
  my $self = shift;
  my $new_id  = shift;
  my $logger = get_logger( "ALMA::User" );
  $logger->debug( "Set new id ", $new_id, "." );
  $self->primary_id = $new_id;
}

sub change_user_identifier {
  # say Dumper(@_);
  my $self = shift;
  my $new_id = shift;
  # my ( $self, $new_id ) = shift;
  say Dumper($new_id);
  my $new_id_type = $new_id->{id_type};
  say "New ID type: ", $new_id_type;
  my %h_ids;
  my $count = "0 but true";
  # say Dumper($new_id);
  # say Dumper($self->{user_identifier});
  foreach my $id ( @{ $self->{user_identifier} } ){
    next unless $id;
    say "existing id: ", Dumper($id);
    my $id_type = $id->{id_type}{value};
    say "id_type is ", $id_type;
    $h_ids{$id_type} = $count;
    say "this is id number ", $count;
    $count++
  }
  say "ID Index: ", Dumper(%h_ids);
  if ( $h_ids{$new_id_type} ){
    say $h_ids{$new_id_type}, " exists already.";
    $self->{user_identifier}->[$h_ids{$new_id->{id_type}}] = $new_id->{hash};
  } else {
    say "ID $new_id_type is new!";
    push @{ $self->{user_identifier} }, $new_id->{hash};
  }
  say "modified ids: ", Dumper($self->{user_identifier});
}

__PACKAGE__->meta->make_immutable;

package ALMA::Role;

use Modern::Perl '2016';
use Moose;
use ALMA::Types ':all';
use utf8::all;

has 'ALMA_role_type' => (
  is  => 'rw',
  # isa => AlmaValue,
  isa => 'HashRef',
  init_arg => 'role_type',
);

has [ 'status', 'scope' ] => (
  is  => 'rw',
  # isa => AlmaValue,
  isa => 'HashRef',
);
has 'parameter' => (
  is  => 'rw',
  isa => 'ArrayRef',
);

sub read_object {
  my $self = shift;
  my $role = {
    role_type => { value => $self->{ALMA_role_type}{value} },
    status    => { value => $self->{status}{value} },
    scope     => { value => $self->{scope}{value} },
    parameter => $self->parameter,
  };
  return $role;
}

1;
