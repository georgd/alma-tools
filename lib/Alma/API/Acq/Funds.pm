package Alma::API::Acq::Funds;

use Modern::Perl '2016';
use utf8::all;

# use Data::Dumper qw( Dumper );
# use Path::Tiny;
use Log::Log4perl qw( :easy );
use XML::LibXML;

use Moose;

extends 'Alma::API::Client';

has 'fund_id' => (
    is        => 'ro',
    isa       => 'Int',
    predicate => 'has_fund_id',
);

has 'fund_code' => (
    is        => 'ro',
    isa       => 'Str',
    predicate => 'has_fund_code',
);

has '+service' => (
    traits  => ['String'],
    is      => 'rw',
    lazy    => 1,
    builder => '_build_service',
    handles => {
        service_append => 'append',
    },
);

has '+params' => (
    traits    => ['Hash'],
    is      => 'rw',
    lazy    => 1,
    builder => '_build_params',
    handles   => {
        set_option     => 'set',
    },
);

sub _build_service {
    my $self = shift;

    my $service = 'acq/funds';
    if ( $self->has_fund_id ) {
        my $fund = $self->fund_id;
        $service .= "/$fund";
    } elsif ($self->has_fund_code){
        my $fund = $self->fund_code;
        $service .= "?q=fund_code~$fund";
    }
    return $service;
}

sub _build_params {
    my $self = shift;
    if ( $self->has_fund_code ) {
        $self->params->set( q => "fund_code~$self->fund_code" );
    }
}

1;
