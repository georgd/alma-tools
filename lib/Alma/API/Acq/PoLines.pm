
package Alma::API::Acq::PoLines;

use Modern::Perl '2016';
use utf8::all;

# use Data::Dumper qw( Dumper );
# use Path::Tiny;
use Log::Log4perl qw( :easy );
use XML::LibXML;

use Moose;

extends 'Alma::API::Client';

has 'po_line_id' => (
    is        => 'ro',
    isa       => 'Int',
    predicate => 'has_po_line_id',
);

has 'item_id' => (
    is        => 'ro',
    isa       => 'Int',
    predicate => 'has_item_id',
);

has '+service' => (
    traits  => ['String'],
    is      => 'rw',
    lazy    => 1,
    builder => '_build_service',
    handles => {
        service_append => 'append',
    },
);

has 'log' => (
    is => 'ro',
    isa => 'Log::Log4perl::Logger',
    default => sub {get_logger("Alma::API::PoLines")},
    lazy => 1,
);

sub _build_service {
    my $self = shift;

    my $service = 'acq/po-lines';
    if ( $self->has_po_line_id ) {
        my $pol = $self->po_line_id;
        $service .= "/$pol";
    }
    return $service;
}

sub get_items {
    my $self = shift;

    $self->service_append('/items');
    $self->get();
}

sub rcv_new_item {
    my $self = shift;

    $self->service_append('/items');
    $self->post();
}

sub rcv_item {
    my $self = shift;

    if ( $self->has_item_id ) {
        $self->service_append("/items/$self->item_id");
        $self->post();
    } else {
        $self->log->warn('No item ID given');
        return;
    }
}
1;
