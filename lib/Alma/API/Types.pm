package ALMA::Types;
use warnings;
use strict;
use MooseX::Types -declare => [
  qw(
    AlmaValue
  )
];

use MooseX::Types::Moose qw/Str HashRef/;

subtype AlmaValue, as Str;

coerce AlmaValue,
  from HashRef,
  via { $_->{value} if defined $_->{value} };

1;
