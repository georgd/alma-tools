package Alma::API::Bibs::Bib;

use Modern::Perl '2016';
use utf8::all;
use Carp;
use Log::Log4perl qw( :easy );
use XML::Writer;
use Data::Dumper;

use Moose;

has 'fields' => (
    traits  => ['Array'],
    is      => 'ro',
    isa     => 'ArrayRef',
    handles => {
        all_fields => 'elements',
        add_field  => 'push',
    },
);

has 'marcxml' => (
    is      => 'ro',
    lazy    => 1,
    builder => '_create_marcxml',
);

has 'format_fields' => (
    is      => 'ro',
    lazy   => 1,
    builder => '_create_marcxml_fields',
);

has '_writer' => (
    is  => 'rw',
    isa => 'XML::Writer',
);

sub _create_marcxml {
    my $self = shift;
    $self->_writer(
        XML::Writer->new(
            OUTPUT      => 'self',
            ENCODING    => 'utf-8',
            DATA_MODE   => 1,
            DATA_INDENT => 2
        )
    );
    $self->_writer->startTag('bib');
    $self->_writer->startTag('record');

    $self->_write_fields( $self->all_fields );
    $self->_writer->endTag('record');
    $self->_writer->endTag('bib');

    return $self->_writer->end();
}

sub _create_marcxcml_fields {
    my $self = shift;
    $self->_writer(
        XML::Writer->new(
            OUTPUT      => 'self',
            ENCODING    => 'utf-8',
            DATA_MODE   => 1,
            DATA_INDENT => 2
        )
    );
    return $self->_write_fields( $self->all_fields );
}

sub _write_controlfield {
    my ( $self, $tag, $content ) = @_;
    carp("Missing input at $tag!") unless $content;
    $self->_writer->dataElement( 'controlfield', $content, tag => $tag );
}

sub _write_datafield {
    my ( $self, $tag, $ind1, $ind2, @subfields ) = @_;
    carp "No content given for $tag!" unless @subfields;
    carp "Uneven number of arguments in $tag!" if scalar @subfields % 2;

    $self->_writer->startTag(
        'datafield',
        ind1 => $ind1,
        ind2 => $ind2,
        tag  => $tag
    );
    while ( my ( $sf, $content ) = splice( @subfields, 0, 2 ) ) {
        $self->_writer->dataElement( 'subfield', $content, code => $sf );
    }
    $self->_writer->endTag('datafield');
}

sub _write_fields {
    my ( $self, @fields ) = @_;
    foreach my $field (@fields) {
        if ( $$field[0] eq 'leader' ) {
            $self->_writer->dataElement( $$field[0], $$field[1] );
        } elsif ( $$field[0] =~ m{^00\d$} ) {
            $self->_write_controlfield(@$field);
        } else {
            $self->_write_datafield(@$field);
        }
    }
}

1;
