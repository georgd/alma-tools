
package Alma::API::Bibs::Holdings;

use Modern::Perl '2016';
use utf8::all;

# use Data::Dumper qw( Dumper );
# use Path::Tiny;
use Log::Log4perl qw( :easy );
use XML::LibXML;

use Moose;

extends 'Alma::API::Client';

has 'mms_id' => (
    is        => 'ro',
    isa       => 'Int',
    predicate => 'has_mms_id',
    required  => 1,
);

has 'holding_id' => (
    is        => 'ro',
    isa       => 'Int',
    predicate => 'has_holding_id',
);

has '+service' => (
    lazy    => 1,
    builder => '_build_service',
);

sub _build_service {
    my $self    = shift;
    
    my $service = 'bibs/' . $self->mms_id;
    if ( $self->has_holding_id ) {
        $service .= '/holdings/' . $self->holding_id;
    } else {
        $service .= '/holdings';
    }
    return $service;
}

1;
