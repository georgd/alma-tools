package Alma::API::Bibs::Items;

use Modern::Perl '2016';
use utf8::all;

# use Data::Dumper qw( Dumper );
use Log::Log4perl qw( :easy );

use Moose;

extends 'Alma::API::Client';

has ['mms_id','holdings_id' ]=> (
    is        => 'ro',
    isa       => 'Int',
    required  => 1,
);

has 'item_pid' => (
    is        => 'ro',
    isa       => 'Int',
    predicate => 'has_item_pid',
);

has '+service' => (
    lazy    => 1,
    builder => '_build_service',
);

sub _build_service {
    my $self = shift;

    my $service = 'bibs/' . $self->mms_id . '/holdings/' . $self->holding_id;
    if ( $self->has_item_pid ) {
        $service .= '/items/' . $self->item_pid;
    } else {
        $service .= '/items';
    }
    return $service;
}

sub scan_in {
    my $self = shift;
    return $self->request('POST');
}

1;
