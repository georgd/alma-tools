
package Alma::API::Bibs;

use Modern::Perl '2016';
use utf8::all;
use Log::Log4perl qw( :easy );
use XML::LibXML;

use Moose;

extends 'Alma::API::Client';

has 'mms_id' => (
    is        => 'ro',
    isa       => 'Int',
    predicate => 'has_mms_id',
);

has '+service' => (
    lazy    => 1,
    builder => '_build_service',
);

has 'content_type' => (
    is      => 'ro',
    isa     => 'Str',
    default => 'application/xml',
);

has 'log' => (
    is => 'ro',
    isa => 'Log::Log4perl::Logger',
    default => sub {get_logger("Alma::API::Bibs")},
    lazy => 1,
);

sub _build_service {
    my $self = shift;

    my $service = 'bibs';
    if ( $self->has_mms_id ) {
        $service .= '/'. $self->mms_id;
    }
    $self->log->warn("Content-Type: ", $self->content_type, "\nService", $service);

    return $service;
}
1;
