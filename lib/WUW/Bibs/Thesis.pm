package WUW::Bibs::Thesis;

use Carp;
use Moose;
use DateTime;
use Log::Log4perl qw( :easy );
use XML::Writer;
use Data::Dumper;
use utf8::all;
use Alma::API::Bibs::Bib;

has [
    'author', 'title',   'lang',     'tr_lang',
    'type',   'studies', 'reviewer', 'local_id'
] => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has 'supervisor' => (
    is        => 'ro',
    isa       => 'Str',
    predicate => 'has_supervisor',
);

has [ 'year', 'year_dipl' ] => (
    is       => 'ro',
    isa      => 'Int',
    required => 1,
);

has 'today' => (
    is      => 'ro',
    isa     => 'DateTime',
    default => sub { DateTime->today },
);

has 'coauthors' => (
    traits    => ['Array'],
    is        => 'rw',
    isa       => 'ArrayRef[Str]',
    predicate => 'has_coauthors',
    handles   => {
        all_coauthors => 'elements',
        add_coauthor  => 'push',
        map_coauthor  => 'map',
    },
);

has 'translated_title' => (
    is        => 'ro',
    isa       => 'Str',
    predicate => 'has_translated_title',
);

has 'bibxml' => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    builder => '_build_bib',
);

around 'BUILDARGS' => sub {
    my $orig  = shift;
    my $class = shift;

    my $args = shift;
    if ( $args->{language} eq 'Englisch' ) {
        $args->{title}   = $args->{title_eng};
        $args->{lang}    = 'eng';
        $args->{tr_lang} = 'ger';
    } else {
        $args->{title}   = $args->{title_ger};
        $args->{lang}    = 'ger';
        $args->{tr_lang} = 'eng';
    }

    if ( $args->{title_eng} ne $args->{title_ger} ) {
        $args->{translated_title} =
            $args->{language} eq 'Englisch'
          ? $args->{title_ger}
          : $args->{title_eng};
    }

    return $class->$orig($args);
};

sub _build_bib {
    my $self = shift;

    my @title_subfields =
      ( a => $self->_normalize_title( $self->title, $self->lang ) );
    push @title_subfields, ( b => "= " . $self->translated_title )
      if $self->has_translated_title;
    push @title_subfields, ( c => $self->_build_authors_list() );

    my @fields = (
        [ 'leader', '#####nam a22##### c 4500' ],
        [ '003',    'AT-UBWW-HSS' ],
        [ '007',    'tu' ],
        [ '008',    $self->_build_008_string() ],
        [ '009',    $self->local_id ],
        [ '040', ' ', ' ', a => 'AT-UBWW', b => 'ger', e => 'rda' ],
        [ '041', ' ', ' ', a => $self->lang ],
        [ '044', ' ', ' ', c => 'XA-AT' ],
        [ '100', '1', ' ', a => $self->author, 4 => 'aut' ],
        [ '245', '0', '0', @title_subfields ]
    );

    if ( $self->has_translated_title ) {
        push @fields,
          [
            '246', '1', '1',
            a => $self->_normalize_title(
                $self->translated_title, $self->tr_lang
            )
          ];
    }

    push @fields,
      (
        [ '264', ' ', '1', a => 'Wien', c => $self->year ],
        [ '336', ' ', ' ', b => 'txt' ],
        [ '337', ' ', ' ', b => 'n' ],
        [ '338', ' ', ' ', b => 'nc' ],
        [
            '502', ' ', ' ',
            b => $self->type,
            c => 'Wirtschaftsuniversität Wien',
            d => $self->year_dipl,
            g => $self->studies
        ],
        [
            '655', ' ', '7',
            a   => 'Hochschulschrift',
            '0' => '(DE-588)4113937-9',
            2   => 'gnd-content'
        ],
      );

    if ( $self->has_coauthors ) {
        foreach my $coauthor ( $self->all_coauthors ) {
            push @fields, [ '700', '1', ' ', a => $coauthor, 4 => 'aut' ];
        }
    }

    push @fields,
      (
        [
            '751', ' ', ' ',
            a   => 'Wien',
            '0' => '(DE-588)4066009-6',
            4   => 'uvp',
            2   => 'gnd'
        ],
        [ '970', '2', ' ', a => 'AT-UBWW', d => $self->_encode_type ],
      );

    if ( $self->has_supervisor ) {
        push @fields, [ '971', '0', ' ', a => $self->supervisor ];
    }

    push @fields,
      (
        [ '971', '1', ' ', a => $self->reviewer ],
        [ '971', '5', ' ', a => 'Wirtschaftsuniversität Wien' ],
        [ '980', '1', ' ', a => 'BACH-Datensatz', 9 => 'LOCAL' ],
      );

    my $bib = Alma::API::Bibs::Bib->new( fields => \@fields );
    return $bib->marcxml;
}

sub _build_008_string {
    my $self     = shift;
    my $today_6c = $self->today->strftime("%y%m%d");

    my $str =
        $today_6c . '|'
      . $self->year
      . '    au      rm    ||| | '
      . $self->lang . ' |';

    return $str;
}

sub _build_authors_list {
    my $self = shift;

    push my @authors, _reverse_names( $self->author );
    if ( $self->has_coauthors ) {
        print "has coauthor\n";
        foreach my $coauthor ( $self->all_coauthors ) {
            print "\tCoauthor: $coauthor\n";
            push @authors, _reverse_names($coauthor);
        }
    }

    return join ' ; ', @authors;
}

sub _reverse_names {
    my $name = shift;
    return join ' ', reverse split ', ', $name;
}

sub _encode_type {
    my $self    = shift;
    my %h_types = ( Masterarbeit => 'HS-MASTER', Dissertation => 'HS-DISS' );

    return $h_types{ $self->type };
}

sub _normalize_title {
    my ( $self, $title, $lang ) = @_;

    my %articles = (
        ger => [qw( der die das des dem den ein eine eines einem einen einer )],
        eng => [qw( the a an )]
    );

    my $regex = join '|', map { "\Q$_\E" } @{ $articles{$lang} };
    $regex = qr/$regex/i;

    $title =~ s/^($regex)\s/<<$1>> /;
    return $title;
}

1;
